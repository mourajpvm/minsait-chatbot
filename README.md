# Minsait ChatBot

This is a project created inside the UpgradeHub Bootcamp.

The ChatBot is inspired on the Minsait contact section by creating an alternative to that contact form. It covers all the required information in a better UI/UX.

This was built using the react library "react-simple-chatbot", avaiable at https://github.com/LucasBassetti/react-simple-chatbot

Deployed with Vercel and avaiable at https://minsait-chatbot.vercel.app/