
import './App.css';
import Steps from './components/steps';
import MainTheme from './components/theme';

function App() {
  return (
    <MainTheme steps={Steps} />
  )
}

export default App;
