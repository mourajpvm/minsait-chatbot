import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ChatBot from 'react-simple-chatbot';
import Steps from './steps';
import Review from './review'
import BotAvatar from '../assets/minChatBot.png'


Review.propTypes = {
  steps: PropTypes.object,
};

Review.defaultProps = {
  steps: Steps,
};

class SimpleForm extends Component {
  render() {
    return (
      <ChatBot
        headerTitle="Minsait ChatBot"
        botDelay="1000"
        botAvatar={BotAvatar}
        steps={Steps}
      />
    );
  }
}

export default SimpleForm;