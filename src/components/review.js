import React, { Component } from 'react';


class Review extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        name: '',
        lastname: '',
        age: '',
        company: '',
        email: '',
        phone: '',
        country: '',
        interest: '',
        subject: '',
        message: '',
  
      };
    }
  
    componentWillMount() {
      const { steps } = this.props;
      const { name, lastname, age, company, email, phone, country, interest, subject, message } = steps;
  
      this.setState({ name, lastname, age, company, email, phone, country, interest, subject, message});
    }
  
    render() {
      const { name, company, interest} = this.state;
      return (
        <div style={{ width: "100%" }}>
          <h3>Summary</h3>
          <table>
            <tbody>
              <tr>
                <td>Name</td>
                <td>{name.value}</td>
              </tr>
              <tr>
                <td>Company</td>
                <td>{company.value}</td>
              </tr>
              <tr>
                <td>Interest</td>
                <td>{interest.value}</td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  }

export default Review;