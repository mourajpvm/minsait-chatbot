import React from 'react';
import { ThemeProvider } from 'styled-components';
import Steps from './steps';
import SimpleForm from './form';

// all available props
const ThemeMinsait = {
  background: '#fae6da',
  fontFamily: 'Helvetica Neue',
  headerBgColor: '#2c658c',
  headerFontColor: '#fff',
  headerFontSize: '20px',
  botBubbleColor: '#6ca8d1',
  botFontColor: '#fff',
  userBubbleColor: '#81cbbe',
  userFontColor: '#4a4a4a',
};

const MainTheme = () => (
    <ThemeProvider theme={ThemeMinsait}>
      <SimpleForm steps={Steps} />
    </ThemeProvider>
  );

export default MainTheme;
