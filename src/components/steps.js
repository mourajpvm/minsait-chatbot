import Review from "./review";

const Steps = [
  {
    id: "1",
    message: "Hi! I am the Minsait´s bot 😉",
    trigger: "2",
  },
  {
    id: "2",
    message: "You can call me MinBot, what is your name?",
    trigger: "name",
  },
  {
    id: "name",
    user: true,
    trigger: "3",
  },
  {
    id: "3",
    message: "Hi {previousValue}, nice to meet you!",
    trigger: "4",
  },
  {
    id: "4",
    message: "And what is your last name?",
    trigger: "lastname",
  },
  {
    id: "lastname",
    user: true,
    trigger: "6",
  },
  {
    id: "6",
    message: "How old are you?",
    trigger: "age",
  },
  {
    id: "age",
    user: true,
    trigger: "7",
    validator: (value) => {
      if (isNaN(value)) {
        return "value must be a number";
      } else if (value < 0) {
        return "value must be positive";
      } else if (value > 100) {
        return `${value}? Come on!`;
      }

      return true;
    },
  },
  {
    id: "7",
    message: "What is your country?",
    trigger: "country",
  },
  {
    id: "country",
    user: true,
    trigger: "8",
  },
  {
    id: "8",
    message: "What is the company you work for?",
    trigger: "company",
  },
  {
    id: "company",
    user: true,
    trigger: "9",
  },
  {
    id: "9",
    message: "Please select an interest of your contact.",
    trigger: "interest",
  },
  {
    id: "interest",
    options: [
      { value: "commercial", label: "Commercial", trigger: "10" },
      { value: "press", label: "Press", trigger: "10" },
      { value: "humanResources", label: "Human Resources", trigger: "10" },
      { value: "partnesCollaborators", label: "Partnes and Collaborators", trigger: "10" },
      { value: "other", label: "Other", trigger: "10" },
    ],
  },
  {
    id: "10",
    message: "And what would be the subject of your contact?",
    trigger: "subject",
  },
  {
    id: "subject",
    user: true,
    trigger: "11",
  },
  {
    id: "11",
    message: "Write down your message to us.",
    trigger: "message",
  },
  {
    id: "message",
    user: true,
    trigger: "12",
  },
  {
    id: "12",
    message: "Nice! I'll personally deliver your message to the right person. 🤖✉️ ",
    trigger: "13",
  },
  {
    id: "13",
    message: "But first, I need to know how to contact you.",
    trigger: "contact",
  },
  {
    id: "contact",
    options: [
      { value: "phone", label: "by Phone 📱", trigger: "14" },
      { value: "email", label: "by Mail 📧 ", trigger: "15" },
    ],
  },
  {
    id: "14",
    message: "Ok, what is your phone number?",
    trigger: "phone",
  },
  {
    id: "phone",
    user: true,
    trigger: "16",
  },
  {
    id: "15",
    message: "Ok, what is your E-mail?",
    trigger: "email",
  },
  {
    id: "email",
    user: true,
    trigger: "16",
  },
  {
    id: "16",
    message: "Great! Check out your summary",
    trigger: "review",
  },
  {
    id: "review",
    component: <Review />,
    asMessage: true,
    trigger: "update",
  },
  {
    id: "update",
    message: "Would you like to update some field?",
    trigger: "update-question",
  },
  {
    id: "update-question",
    options: [
      { value: "yes", label: "Yes", trigger: "update-yes" },
      { value: "no", label: "No", trigger: "end-message" },
    ],
  },
  {
    id: "update-yes",
    message: "What field would you like to update?",
    trigger: "update-fields",
  },
  {
    id: "update-fields",
    options: [
      { value: "name", label: "Name", trigger: "update-name" },
      { value: "lastname", label: "Last Name", trigger: "update-lastname" },
      { value: "age", label: "Age", trigger: "update-age" },
      { value: "company", label: "Company", trigger: "update-company" },
      { value: "email", label: "E-mail", trigger: "update-email" },
      { value: "phone", label: "Phone", trigger: "update-phone" },
      { value: "country", label: "Country", trigger: "update-country" },
      { value: "interest", label: "Interest", trigger: "update-interest" },
      { value: "subject", label: "Subject", trigger: "update-subject" },
      { value: "message", label: "Message", trigger: "update-message" },
    ],
  },
  {
    id: "update-name",
    update: "name",
    trigger: "16",
  },
  {
    id: "update-lastname",
    update: "lastname",
    trigger: "16",
  },
  {
    id: "update-age",
    update: "age",
    trigger: "16",
  },
  {
    id: "update-company",
    update: "company",
    trigger: "16",
  },
  {
    id: "update-email",
    update: "email",
    trigger: "16",
  },
  {
    id: "update-phone",
    update: "phone",
    trigger: "16",
  },
  {
    id: "update-interest",
    update: "interest",
    trigger: "16",
  },
  {
    id: "update-subject",
    update: "subject",
    trigger: "16",
  },
  {
    id: "update-message",
    update: "message",
    trigger: "16",
  },
  {
    id: "update-country",
    update: "country",
    trigger: "16",
  },
  {
    id: "end-message",
    message: "Thanks! Your data was submitted successfully!",
    end: true,
  },
];

export default Steps;

